# Spring Simple Lab
***

## 使用技術


1. **[Spring Core](https://docs.spring.io/spring-framework/docs/current/spring-framework-reference/core.html)**
2. **[Spring Web MVC](https://docs.spring.io/spring/docs/current/spring-framework-reference/web.html)**
3. **[Spring Data JPA](https://docs.spring.io/spring-data/jpa/docs/2.1.3.RELEASE/reference/html/)**
4. **[Spring AOP](https://docs.spring.io/spring-framework/docs/current/spring-framework-reference/core.html#aop)**
5. **[Spring Cache Caffeine](https://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-caching.html#boot-features-caching-provider-caffeine)**
6. Java 8
	+ **[Lambda](https://magiclen.org/java-8-lambda/)**
	+ **[Optional API](http://blog.tonycube.com/2015/10/java-java8-4-optional.html)**
	+ **[Stream API](https://www.ibm.com/developerworks/cn/java/j-lo-java8streamapi/index.html)**
	+ **[CompletableFuture](https://popcornylu.gitbooks.io/java_multithread/content/async/cfuture.html)**
7. **[Spring Boot 2](https://docs.spring.io/spring-boot/docs/2.1.1.RELEASE/reference/htmlsingle/)**
8. **[Spring Transaction](https://docs.spring.io/spring-framework/docs/current/spring-framework-reference/data-access.html)**
9. **[BCrypt](https://ithelp.ithome.com.tw/articles/10196477)**
10. **[Spring Security](https://docs.spring.io/spring-security/site/docs/5.1.2.RELEASE/reference/htmlsingle/)**
11. **[JWT(JSON Web Token)](https://yami.io/jwt/)**
12. **[ECDSA](https://hk.saowen.com/a/34562609077f8b590a14b571c1cfa9412bf4277eda6f027d979a1e21bafd8e36)**
13. **[Spring Schedule](https://www.baeldung.com/spring-scheduled-tasks)**
14. **[Cron Expressions](https://www.baeldung.com/cron-expressions)**

***
## RESTful API 架構


![](https://i.imgur.com/fw4MI8D.png)
***

## 開發環境設定

+ 安裝 JAVA jdk 8
+ 安裝 Git
***

## 專案結構

### Package

+ **aop：** aop 相關實作
+ **auth：** 身份驗證相關實作
+ **bo：** 業務邏輯物件
+ **config：** Spring 相關配置
+ **constant：** 常數定義
+ **controller：** Controller 定義
+ **dao：** 資料存取物件
+ **dto：** 資料傳輸物件
+ **error：** 自定義例外
+ **factory：** 工廠物件
+ **po：** 持久物件（資料庫資料 mapping）
+ **scheduler：** 排程相關實作
+ **service：** 業務邏輯實作
+ **util：** 共用元件實作
+ **validator：** 資料檢核相關實作

***

## 快速啟動

### Step 1：將專案 clone 下來

```shell
$ git clone https://gitlab.com/Phoenix-III-Java/mocksite.git
```

### Step 2：使用 Eclipse 開啟專案

![](https://i.imgur.com/wSvgTAP.png)

![](https://i.imgur.com/kiamXLK.png)

### Step 3：Import 為 Gradle 專案

![](https://i.imgur.com/snu7y2a.png)

![](https://i.imgur.com/i01cKop.png)

### Step 4：執行 com.example.demo.DemoApplication.java

![](https://i.imgur.com/77yfS2A.png)

### Step 5：應用程式啟動後會自動啟動內嵌的 H2 資料庫

### Step 6：自動根據 com.example.demo.po 裡定義之物件內容初始化資料庫的資料表

### Step 7：自動根據 src/main/resources/data-h2.sql 裡定義之 sql 來建立初始資料

### Step 8：http://localhost:8080/h2 開啟 DB 控制台

![](https://i.imgur.com/96H5TVZ.png)

***

## 身份驗證機制

### 預設 http://localhost:8080/user 相關 API 皆需經過驗證才能使用。

### Step 1：透過 API http://localhost:8080/auth/login 進行登入，成功將回傳 Token

+ 預設帳號：admin
+ 預設密碼：admin

>**NOTE：** 密碼需先經過 RSA 加密，可透過 API http://localhost:8080/demo 進行加密

![](https://i.imgur.com/awaPFUL.png)

![](https://i.imgur.com/VU3YiLV.png)

### Step 2：使用登入取得的 Token 帶入 Http request header「Authorization」即可呼叫 user 相關 API

![](https://i.imgur.com/nWirdYX.png)


***

## 開始前動作


### Step 1：從 這個Repository Fork出自己的開發Repository


### Step 2：開始開發
***
## LAB 目標
***

### 1. 按照以下 API 規格進行開發

>**NOTE：** 不需要快取

#### 1-1. API：查詢所有書籍

##### 基本資訊

|                   | Content         |
| ----------------- | --------------- |
| **URL**           | /book           |
| **HTTP Method**   | GET             | 
| **Desc**          | 查詢所有的 book   |

##### 回覆（Response）訊息格式

| Column              | Type      | Remarks             |
| ------------------- | -------   | ------------------- |
| Repeating Structure |         |                     |
| name                | String    | 書名                 |
| author              | String    | 作者                 |
| publicationDate     | String    | 出版日期 yyyy-mm-dd   |

#### 1-2. API：以書名查詢書籍

##### 基本資訊

|                   | Content           |
| ----------------- | ----------------- |
| **URL**           | /book/{name}      |
| **HTTP Method**   | GET               | 
| **Desc**          | 以 name 撈取 book  |

##### 回覆（Response）訊息格式

| Column            | Type      | Remarks             |
| ----------------- | -------   | ------------------- |
| name              | String    | 書名                 |
| author            | String    | 作者                 |
| publicationDate   | String    | 出版日期 yyyy-mm-dd   |

#### 1-3. API：新增書籍

##### 基本資訊

|                   | Content                                                       |
| ----------------- | ------------------------------------------------------------- |
| **URL**           | /book                                                         |
| **HTTP Method**   | POST                                                          | 
| **Desc**          | 1. 將請求（Request）的書籍資料寫入 Table「BOOK」2. 回傳已新增的書籍資料 |

##### 請求（Request）訊息格式

| Column            | Type      | Remarks            |
| ----------------- | -------   | -------------------|
| name              | String    | 書名                |
| author            | String    | 作者                |
| publicationDate   | String    | 出版日期 yyyymmdd   |

##### 回覆（Response）訊息格式

| Column            | Type      | Remarks             |
| ----------------- | -------   | ------------------- |
| name              | String    | 書名                 |
| author            | String    | 作者                 |
| publicationDate   | String    | 出版日期 yyyy-mm-dd   |

#### 1-4. API：刪除書籍

##### 基本資訊

|                   | Content           |
| ----------------- | ----------------- |
| **URL**           | /book/{name}      |
| **HTTP Method**   | DELETE            | 
| **Desc**          | 以 name 來刪除書籍  |

#### 1-5. API：更新書籍

##### 基本資訊

|                   | Content                                                                                           |
| ----------------- | -----------------------------------------------------------------------------------------------   |
| **URL**           | /book                                                                                             |
| **HTTP Method**   | PUT                                                                                               | 
| **Desc**          | 1. 將請求（Request）的書籍資料更新對應書名的書籍資料 2. 若請求欄位為 null，則不更新此欄位 3. 回傳已更新的書籍資料   |

##### 請求（Request）訊息格式

| Column            | Type      | Remarks            |
| ----------------- | -------   | -------------------|
| name              | String    | 書名                |
| author            | String    | 作者                |
| publicationDate   | String    | 出版日期 yyyymmdd   |

##### 回覆（Response）訊息格式

| Column            | Type      | Remarks             |
| ----------------- | -------   | ------------------- |
| name              | String    | 書名                 |
| author            | String    | 作者                 |
| publicationDate   | String    | 出版日期 yyyy-mm-dd   |

### 2. 針對lab01 寫單元測試
  - 新增(Create/Insert)測試案例
  - 查詢(Read/Query)測試案例
  - 修改(Update)測試案例
  - 刪除(Delete)測試案例

### 3. 針對資源 Book 新增 Cache

#### 3-1. Cache 時間為 1 分鐘
#### 3-2. 查詢時若有Cache則回傳Cache，若沒Cache則重新查詢後更新Cache並回傳
#### 3-3. 變更時更新Cache
#### 3-4. 刪除時清除Cache
***

### 4. Transaction 控制

#### 4-1. 設計一API：同時新增使用者、書籍，並以 Transaction 控制若有任何例外發生，則 rollback
#### 4-2. 設計一API：同時更新使用者、書籍，並以 Transaction 控制若有任何例外發生也不影響已經執行的 DB 操作
#### 4-2. 設計一API：同時更新使用者、書籍，並以 Transaction 控制若有任何例外發生也不影響已經執行的 DB 操作，使用Stored Procedures

***

### 5. AOP (Aspect-Oriented Programming)

>**NOTE：** 使用 AOP 的方式實作

#### 5-1. 在進到 Controller method 前印出「{method 名稱} 開始執行」、輸入的參數
#### 5-2. 在 Controller method 之後印出「{method 名稱} 執行結束」
#### 5-3. 印出執行 Controller method 所花費的時間
#### 5-4. 印出執行 Controller method 到 Service 所花費的時間
#### 5-5. 印出執行 Service method 到 DAO所花費的時間
#### 5-6. 印出執行 DAO method 所花費的時間
***

### 6. Security

>**NOTE：** 6-1 使用 Spring Security 實作

#### 6-1. 將 Book API 限制為角色「USER」才能存取
#### 6-2. 實作一個方法將一個字串做Base64編碼
```java
String stringToBase64String(String inputString)
```
#### 6-3. 實作一個方法將byte[]做Base64編碼
```java
String bytesToBase64String(byte[] bytes)
```
#### 6-4. 實作一個方法將Base64字串做Base64解碼，並印出byte[]的長度
```java
void decodeBase64String(String base64String)
```
#### 6-5. 調查並分享目前企業應用上常用的加解密有哪些種類、哪些演算法？
***

### 7. Scheduler

>**NOTE：** 使用 Spring Schedule 的方式實作

#### 7-1. 新增一排程每10秒鐘統計一次當前DB裡有多少本書，並在console log印出

#### 7-2. 了解Cron expression

#### Spring Schedule 結合 Quartz

#### Spring Schedule 結合 Spring Batch
***

### 8. Request Validate

>**NOTE：** 遵循專案架構並使用專案中的 @Validator 實作 request 檢核

#### 8-1. 除 API「更新書籍」外，其餘 API request 參數皆為必填
#### 8-2. 使用 @Validator 給定正規表示式來檢核 Book API request 中所有書名「只能是英數字」、「長度最大為 Table 定義之最大長度」
#### 8-3. 使用 @Validator 給定正規表示式來檢核 Book API request 中所有作者「只能是英文字」、「長度最大為 Table 定義之最大長度」
#### 8-4. 使用 @Validator 給定參數 validator 來檢核 Book API request 中所有出版日期「符合規格中定義之格式」
***

### 9. 修改 Book API：查詢所有書籍

#### 9-1. 修改為可帶「order」參數，值必須為「name」、「author」、「date」其中之一，非必填，例如：/book?order=name
#### 9-2. 根據參數 order 決定回傳的 book list 以哪個欄位進行排序
+ name：書名以「長->短」排序
+ author：作者以英文字母「A -> Z」排序
+ date：出版日期「新 -> 舊」排序

***

### 10. Lambda

#### 10-1. 說明 Lambda 的概念、使用方法
+ 優點
+ 使用條件
+ 舉例

***

### 11. Stream

#### 11-1. 將 Book API：查詢所有書籍，對集合的操作改為使用 Stream的方式

***
#### 12. Update 強化
* 修正Lab 1  Update 時若有欄位值為Null，依然可過Service，但不更改該欄位資料
***
#### 13. Cache Flow
* 修正Lab 2  若有新增/刪除/修改的行為時，需對Cache的內容做變動
***
#### 14. AOP 強化
* 精簡化 AOP 的應用，使用一個@Around即可完成Lab 5的要求
***
#### 15. Secret Flow
* 修正Lab 6 輸入進資料庫的使用者密碼須加密處理，並且需要考慮加鹽(Salt)
* 
***

#### 16. PassWord and Salt Flow
![](https://image.slidesharecdn.com/pssw0rds-180317214220/95/pssw0rds-22-1024.jpg?cb=1521323107)

#### 知名的密碼雜湊的破解成本比較
![](https://i.stack.imgur.com/6Iwa2.png)

***

#### 17. Spring Interceptor
### 17.1 說明 Interceptor 的概念、使用方法
+ 優點
+ 使用條件
+ 舉例
***
#### 18. Java Benchmark
### 18.1 說明 JMH 的概念、使用方法
+ 優點
+ 使用條件
+ 舉例
***
#### 19. Docker
### 19.1 說明 Docker 的概念、使用方法
+ 優點
+ 使用條件
+ 舉例

### 19.2 將H2改為一般RDBMS(docker)

### 19.3 將19.2改成docker-compose 
***
#### 20 Security 進階 JWT,Session,OAuth
### 20.1 說明 JWT,Session,OAuth 的概念、使用方法
+ 優點
+ 使用條件
+ 舉例
### 20.2 結合Spring Security
***
#### 21 Test
### 21.1 UnitTest
+ Dummy
+ Fake
+ Stub
+ Mock
+ Spy
### 21.2 IntegrationTest

---
### 22
- 人員資料(User)會有以下欄位資訊需要儲存：
  - 帳戶名稱(AccountName, varchar(30) character set UTF8)
  - 顯示名稱(DisplayName, varchar(30) character set UTF8)
  - 年齡(Age, int)
  - 連絡信箱(Email, varchar(50) character set UTF8)
  - 備註(Memo, varchar(100) character Userset UTF8, nullable)
  
- 實作資料操作的功能開發：
  - 新增(Create/Insert)
  - 查詢(Read/Query)
  - 修改(Update)
  - 刪除(Delete)

人員資料的帳目名稱只可以輸入英文、數字與底線，顯示名稱可為中英文數字與特定特殊字元(底線與空白)

## 資料庫圖表

![UserTable](Lab_1_User.png)

---

- 新增四個單元測試案例，分別進行以下操作測試：
  - 新增(Create/Insert)測試案例
  - 查詢(Read/Query)測試案例
  - 修改(Update)測試案例
  - 刪除(Delete)測試案例


- 新增一個人員群組資料(Group), 有以下分類：
  - Admin
  - Agent
  - User
- 人員資料(User)新增以下欄位資訊需要儲存，並使用上面的分類資料塞入：
  - 群組(Group)
- 人員資料(User)調用從 `PL/SQL` 呼叫改為使用 `Store Procedure`

## 資料庫圖表

![UserTable](Lab_3_UserGroup.png)

***

寫下必要的測試，透過 Interface 與 `Stub` 手法進行測試
  
[Test Doubles — Fakes, Mocks and Stubs.](https://blog.pragmatists.com/test-doubles-fakes-mocks-and-stubs-1a7491dfa3da)
[深入探討 Test Double、Dummy、Fake、Stub 、Mock 與 Spy](https://oomusou.io/jasmine/jasmine-test-double/))

***

### 23.針對Restful改為傳統Get&Post

#### 2-1 查詢使用Get
#### 2-2 其餘使用Post

***

### 24.針對Restful改為GRPC

#### 3-1 Proto3
#### 3-2 Grpc

***